from django.urls import path
from .views import feed, userpage, userstatus, StatusCreateView

urlpatterns = [
    path('', feed, name="feed"),
    path('konfirmasi/', StatusCreateView.as_view(), name="konfirmasi"),
    path('@<str:user>/', userpage, name="userpage"),
    path('@<str:user>/<int:status_id>/', userstatus, name="userstatus")
]
