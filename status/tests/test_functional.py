from selenium import webdriver
from status.models import Orang, Status
from status.utils import url_with_querystring
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time
import os

class TestFunctional(StaticLiveServerTestCase):

    def setUp(self):
        super().setUp()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.close()
        super().tearDown()

    def test_no_projects(self):
        self.browser.get(self.live_server_url)
        get_item = self.browser.find_element_by_css_selector("[data-test=alert]")
        self.assertEqual(
            get_item.text,
            "There is no status on the feed."
        )

    def test_form_redirect_to_konfirmasi(self):
        self.browser.get(self.live_server_url)
        querystring = {
            'nama': 'budi',
            'email': 'b@b.com',
            'pesan': 'halo budi'
        }

        to_url = self.live_server_url + url_with_querystring(reverse('konfirmasi'), **querystring)

        form_nama = self.browser.find_element_by_id("nama")
        form_email = self.browser.find_element_by_id("email")
        form_pesan = self.browser.find_element_by_id("pesan")

        form_nama.send_keys(querystring['nama'])
        form_email.send_keys(querystring['email'])
        form_pesan.send_keys(querystring['pesan'])

        self.browser.find_element_by_id("addsubmit").click()

        self.assertEqual(
            self.browser.current_url,
            to_url
        )

    def test_form_konfirmasi_accept(self):
        self.browser.get(self.live_server_url)
        querystring = {
            'nama': 'budi',
            'email': 'b@b.com',
            'pesan': 'halo budi'
        }

        to_url = self.live_server_url + reverse('feed')

        form_nama = self.browser.find_element_by_id("nama")
        form_email = self.browser.find_element_by_id("email")
        form_pesan = self.browser.find_element_by_id("pesan")

        form_nama.send_keys(querystring['nama'])
        form_email.send_keys(querystring['email'])
        form_pesan.send_keys(querystring['pesan'])

        self.browser.find_element_by_id("addsubmit").click()

        self.browser.find_element_by_id("kirim").click()

        obj_budi = Orang.objects.filter(nama=querystring['nama']).first()

        self.assertEqual(
            self.browser.current_url,
            to_url
        )

        self.assertEqual(
            obj_budi.email,
            "b@b.com"
        )

        self.assertEqual(
            obj_budi.status.all().first().pesan,
            "halo budi"
        )

    def test_form_konfirmasi_kembali(self):
        self.browser.get(self.live_server_url)
        querystring = {
            'nama': 'andi',
            'email': 'sapi@sapi.com',
            'pesan': 'halo budi'
        }

        to_url = self.live_server_url + reverse('feed')

        form_nama = self.browser.find_element_by_id("nama")
        form_email = self.browser.find_element_by_id("email")
        form_pesan = self.browser.find_element_by_id("pesan")

        form_nama.send_keys(querystring['nama'])
        form_email.send_keys(querystring['email'])
        form_pesan.send_keys(querystring['pesan'])

        self.browser.find_element_by_id("addsubmit").click()

        self.browser.find_element_by_id("kembali").click()

        obj_andi = Orang.objects.filter(nama=querystring['nama'])

        self.assertEqual(
            self.browser.current_url,
            to_url
        )

        self.assertEqual(
            obj_andi.count(),
            0
        )
