from django.test import SimpleTestCase
from django.urls import reverse, resolve
from status import views

class TestUrls(SimpleTestCase):

    def test_feed_url_resolved(self):
        url = reverse('feed')
        self.assertEquals(resolve(url).func, views.feed)

    def test_konfirmasi_url_resolved(self):
        url = reverse('konfirmasi')
        self.assertEquals(resolve(url).func.view_class, views.StatusCreateView)

    def test_user_url_resolved(self):
        url = reverse('userpage',args=['budi'])
        print(url)
        self.assertEquals(resolve(url).func, views.userpage)

    def test_userstatus_url_resolved(self):
        url = reverse('userstatus',args=['budi', '120200901'])
        print(url)
        self.assertEquals(resolve(url).func, views.userstatus)

