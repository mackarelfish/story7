from django.test import TestCase
from django.utils.text import slugify
from status.models import Orang, Status

from datetime import datetime

class TestModel(TestCase):
    def setUp(self):
        self.orang1 = Orang.objects.create(
            nama='Budi',
            email='w@w.com'
        )

        self.status1 = Status.objects.create(
            pesan='saya budi',
            orang= self.orang1
        )

    def test_adding_user_model(self):
        self.assertEqual(Orang.objects.all().count(), 1)

    def test_adding_status_model(self):
        self.assertEqual(Status.objects.all().count(), 1)

    def test_check_date_posted(self):
        self.assertEqual(self.status1.date_posted.ctime(), datetime.now().ctime())
