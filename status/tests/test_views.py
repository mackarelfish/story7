from django.test import TestCase, Client
from django.urls import reverse
from status.models import Orang, Status

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.orang1 = Orang.objects.create(nama="andi", email="a@a.com")
        self.status1 = Status.objects.create(orang=self.orang1, pesan="Halo pren")

    def test_page_feed_GET(self):
        response = self.client.get(reverse('feed'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status/index.html')

    def test_page_userpage_GET(self):
        response = self.client.get(reverse('userpage', args=[self.orang1.nama]))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status/userpage.html')

    def test_page_userstatus_GET(self):
        response = self.client.get(reverse('userstatus', args=[self.orang1.nama, self.status1.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status/userstatus.html')

    def test_page_konfirmasi_GET(self):
        response = self.client.get(reverse('konfirmasi'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status/konfirmasi.html')

    def test_page_feed_redirect_to_confirm_POST(self):
        response = self.client.post(reverse('feed'), {
            'nama': 'Budi',
            'email': 'b@b.com',
            'pesan': 'saya budie'
        })
        self.assertEqual(response.status_code, 302)
