from django.shortcuts import render, redirect, reverse
from django.views.generic import CreateView
from status.models import Status, Orang
from status.utils import url_with_querystring

# Create your views here.
def feed(request):
    if request.method == "POST":
        res = request.POST
        querystring = {
            'nama': res.get('nama'),
            'email': res.get('email'),
            'pesan': res.get('pesan'),
        }
        return redirect(url_with_querystring(reverse('konfirmasi'), **querystring))

    context = {
        "data": Status.objects.all()
    }

    return render(request, "status/index.html", context)

def userpage(request, user):
    getUser = Orang.objects.filter(nama=user).first()
    getUserStatus = ''
    if getUser:
        getUserStatus = getUser.status.all()
    context = {
        "user": user,
        "status": getUserStatus
    }
    return render(request, "status/userpage.html", context)

def userstatus(request, user, status_id):
    getStatus = Status.objects.filter(pk=status_id).first()
    context = {
        "status": getStatus
    }
    return render(request, "status/userstatus.html", context)

class StatusCreateView(CreateView):
    model = Status
    template_name = "status/konfirmasi.html"
    fields = ('pesan',)

    def form_valid(self, form):
        post_data = self.request.POST
        nama = post_data.get('nama')
        email = post_data.get('email')
        orang = Orang.objects.filter(email=email).first()
        if not orang:
            orang = Orang.objects.create(nama=nama, email=email)

        Status.objects.create(orang=orang, pesan=form.cleaned_data['pesan'])

        return redirect('feed')
