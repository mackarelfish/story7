from urllib.parse import urlencode

def url_with_querystring(path, **kwargs):
    return path + '?' + urlencode(kwargs)
