from django.db import models

# Create your models here.
class Orang(models.Model):
    nama = models.CharField(max_length=20)
    email = models.EmailField(max_length=75, unique=True)

class Status(models.Model):
    orang = models.ForeignKey(Orang, on_delete=models.CASCADE, related_name="status")
    pesan = models.TextField()
    date_posted = models.DateTimeField(auto_now_add=True)
